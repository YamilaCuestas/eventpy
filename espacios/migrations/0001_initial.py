# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-13 16:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Espacio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_evento', models.CharField(max_length=10, unique=True)),
                ('nombre', models.CharField(blank=True, max_length=50, null=True, verbose_name='Nombre del Espacio para mostrar')),
                ('tipo_espacio', models.CharField(choices=[('Aula', 'Aula'), ('Auditorio', 'Auditorio'), ('Espacio Abierto', 'Espacio Abierto'), ('Quincho', 'Quincho'), ('Salón', 'Salón'), ('Laboratorio', 'Laboratorio'), ('No Especificado', 'No Especificado')], default='No Especificado', max_length=20)),
                ('descripcion', models.TextField(blank=True, null=True, verbose_name='Descripción del espacio')),
                ('cantidad_maxima', models.IntegerField(blank=True, null=True)),
                ('proyector', models.BooleanField(default=False)),
                ('cantidad_computadoras', models.IntegerField(blank=True, null=True)),
                ('lugar', models.CharField(blank=True, max_length=100, null=True, verbose_name='Especifica el Lugar')),
                ('cupo', models.IntegerField(blank=True, null=True)),
                ('foto', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Foto del espacio')),
            ],
        ),
    ]
