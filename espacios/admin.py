from django.contrib import admin
from .models import Espacio


@admin.register(Espacio)
class EspacioAdmin(admin.ModelAdmin):
	list_display = ('id_espacio','nombre','tipo_espacio','cantidad_maxima','proyector','cantidad_computadoras','lugar','cupo')
	search_fields = ('nombre',)


