from django.db import models
from django.utils import timezone

class Espacio(models.Model):
    AULA = 'Aula'
    AUDITORIO ="Auditorio"
    ABIERTO = "Espacio Abierto"
    QUINCHO = "Quincho"
    SALON = "Salón"
    LABORATORIO = "Laboratorio"
    NO_ESPECIFICADO = "No Especificado"
    TIPO_ESPACIO = (
        (AULA, 'Aula'),
        (AUDITORIO,"Auditorio"),
        (ABIERTO, "Espacio Abierto"),
        (QUINCHO, "Quincho"),
        (SALON, "Salón"),
        (LABORATORIO, "Laboratorio"),
        (NO_ESPECIFICADO, "No Especificado"),
        )

    id_espacio = models.CharField(max_length=10,unique=True, blank=False, null=False)
    nombre = models.CharField("Nombre del Espacio para mostrar", max_length=50, blank=True, null=True)
    tipo_espacio = models.CharField(max_length=20, choices= TIPO_ESPACIO, default=NO_ESPECIFICADO)
    descripcion = models.TextField("Descripción del espacio", blank=True, null=True)
    cantidad_maxima = models.IntegerField(blank=True, null=True)
    proyector = models.BooleanField(blank=False, default=False)
    cantidad_computadoras = models.IntegerField(blank=True, null=True)
    lugar = models.CharField("Especifica el Lugar", max_length=100, blank=True, null=True)
    cupo = models.IntegerField(blank=True, null=True)
    foto = models.ImageField("Foto del espacio",  blank=True, null=True, upload_to='espacios/')

    def __str__(self):
        return self.nombre
