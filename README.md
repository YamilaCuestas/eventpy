# EventPy
#### Aplicación web para Organizar un Evento


El desarrollo de EventPy fue iniciado como trabajo de final de la asignatura __Programación 3__ de la carrera __Lic.en Sistemas de Información - Universidad Nacional de Luján__.
 
El objetivo del sistema es dar soporte a la realización y organización de eventos, brindando funcionalidades que permitan la administración de  las cuestiones conocidas que involucran este tipo de actividades. 

## CONTENIDO

> [1. Instalación de entorno de Desarrollo](#1-instalación-de-entorno-de-desarrollo)

> [2. Aplicaciones de EventPy](#2-módulos-de-eventpy)

>> [2.1. Usuarios](#21-módulo-de-gestión-de-usuarios)

>> [2.2. Evento](#22-módulo-de-gestión-de-evento)

>> [2.3. Espacios](#23-módulo-de-gestión-de-espacios)

>> [2.4. Sponsors ](#24-módulo-de-gestión-de-sponsors)

>> [2.5. Actividades](#25-módulo-de-gestión-de-actividades)

> [3. Proyecto EventPy](#3-proyecto-eventpy)

>> [3.1. Descripción del Proyecto](#31-descripción-del-proyecto)

>> [3.2. Objetivos](#32-objetivos-o-propósito)

>> [3.3. Beneficios](#33-beneficios)

>> [3.4. Alcance](#34-alcance)

>> [3.5. Limitaciones](#35-limitaciones)

> [4. Líneas Futuras](#4-líneas-futuras)



### 1. Instalación de entorno de Desarrollo

* Creación de un entorno virtual:

``` fish
    $virtualenv -p python3 eventpy
```

* Luego dentro de la carpeta creada por el entorno virtual, clonar el repositorio:

``` fish
    ~/eventpy$ git clone https://@gitlab.com/YamilaCuestas/eventpy.git

```

> (puede obviarse la creacion del entorno virtual pero es recomendable)
> Para saber mas sobre Virtualenv: [https://virtualenv.pypa.io/en/stable/](https://virtualenv.pypa.io/en/stable/)

* Activar entorno virtual :

``` fish
    ~/eventpy$
    ~/eventpy$ . bin/activate

```

* Dentro de la carpeta __eventpy__ _(La que fue generada al clonar el repositorio)_ ,instalar todas las dependencias de la aplicación

``` fish
    ~/eventpy/eventpy/$ pip3 install -r requirements.txt
```


__ Se utiliza la fish y el entorno virtual (eventpy) __

* Se genera el modelo de datos

``` fish
    (eventpy)yamila@jake ~/e/eventpy> python3 manage.py migrate
    Operations to perform:
      Apply all migrations: admin, auth, contenttypes, eventos, sessions
    Running migrations:
      Applying contenttypes.0001_initial... OK
      Applying auth.0001_initial... OK
      Applying admin.0001_initial... OK
      Applying admin.0002_logentry_remove_auto_add... OK
      Applying contenttypes.0002_remove_content_type_name... OK
      Applying auth.0002_alter_permission_name_max_length... OK
      Applying auth.0003_alter_user_email_max_length... OK
      Applying auth.0004_alter_user_username_opts... OK
      Applying auth.0005_alter_user_last_login_null... OK
      Applying auth.0006_require_contenttypes_0002... OK
      Applying auth.0007_alter_validators_add_error_messages... OK
      Applying auth.0008_alter_user_username_max_length... OK
      Applying eventos.0001_initial... OK
      Applying eventos.0002_evento_id_evento... OK
      Applying eventos.0003_auto_20171123_1456... OK
      Applying sessions.0001_initial... OK

```

* Carga inicial del modelo

``` fish
    (eventpy)yamila@jake ~/e/eventpy> python3 manage.py loaddata eventos.json
    Installed 1 object(s) from 1 fixture(s)
    (eventpy)yamila@jake ~/e/eventpy> python3 manage.py loaddata duracion_temas.json
    Installed 14 object(s) from 1 fixture(s)

```


* Generar el superusuario para el admin

```
    (eventpy)yamila@jake ~/e/eventpy> python manage.py createsuperuser

```

* Ejecutar el servidor e Ingresar al admin (localhost:8000/admin)

```
    (eventpy)yamila@jake ~/e/eventpy> python3 manage.py runserver
    Performing system checks...

    System check identified no issues (0 silenced).
    December 01, 2017 - 10:58:43
    Django version 1.11.2, using settings 'eventpy.settings'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.


```

#### DESARROLLADOR

#### Cuando se bajen los cambios debe llevarse a cabo un _migrate_ por si hubo cambios en el modelo de datos

### 2. Módulos de EventPy

``` fish
    ~/eventpy/eventpy/ls
        actividades  
        eventos       
        eventpy  
        manage.py  
        requirements.txt  
        static     
        usuarios
        espacios             
        eventos.json  
        LICENSE  
        README.md  
        sponsors          
        templates

```

#### 2.1. Módulo  de Gestión de Usuarios 

App desarrollada para EventPy que provee las funcionalidades para manejar Disertantes, Organizadores y Usuarios en general. 

Este módulo permite administrar usuarios, organizadores, disertantes y administradores. Tiene las funcionalidades de registrar,modificar y eliminar de cada uno de los perfiles y maneja los permisos que correspondan según el perfil asignado. 

#### 2.2. Módulo  de Gestión de Evento.

El módulo de Gestión de Evento se compone una única app que implementa todas las funcionalidades necesarias para la administración del evento que se lleve a cabo. La aplicación de Eventos permite registrar el evento e ir modificando los datos necesarios en la medida que se desarrollan los distintos hitos relacionados con un evento o conferencia. EventPy permite ir habilitando hitos conocidos como “Llamado a Charlas”, “Inscripciones” y “publicación de Cronograma”, los cuales irán habilitando otras funcionalidades presentes en el sistema. 

Además, la aplicación eventos permite al administrador modificar la visualización de los distintos contenidos para el resto de los usuarios, funcionalidad que convierte a EventPy en una aplicacion de gestión de eventos dinámica y adaptable a las necesidades de los organizadores del evento. 

El módulo de Gestión de Evento de EventPy implementa funcionalidades centrales:

- Modificación los datos del  Evento.
- Definir los días del Evento.
- Definir el lugar específico y ciudad donde se desarrolla. 
- Habilitar el hito de Llamado a Charlas para los disertantes.
- Habilitar el hito de Inscripciones que permite registrarse en el sitio web.
- Habilitar el hito de Publicar Cronograma que permite mostrar el cronograma de actividades aprobadas.  
- Habilitar la opción de Mostrar Sponsors en el sitio web del evento.
- Habilitar la opción de Mostrar Organizadores en el sitio web del evento.
- Habilitar la opción de Mostrar Disertantes en el sitio web del evento.
- Mostrar el index del sitio con todos los datos definidos y habilitados.
- Mostrar el panel de usuario. 
- Mapear las distintas funcionalidades del resto de las aplicaciones de EventPy


#### 2.3. Módulo  de Gestión de  Espacios
El modulo de Gestión de Espacios permite administrar los distintos espacios disponibles para el evento. Permite dar de alta nuevos espacios con todos los datos necesarios, modificar espacios existentes y eliminar aquellos siempre que sea posible llevar a cabo esta acción. La app de espacios solo implementa models.py y admin.py 

#### 2.4. Módulo  de Gestión de Sponsors
La aplicación de Sponsors permite al administrador dar de alta nuevos sponsors, modificar los datos, eliminar sponsors y habilitarlos para que sean mostrados en el sitio web del evento.  La app de espacios solo implementa models.py y admin.py 


#### 2.5. Módulo  de Gestión de Actividades

El modulo de Gestion de Actividades permite a los administradores dar de alta nuevas actividades, modificarla o eliminarlas. Además permite a los disertantes presentar propuestas de charlas que serán luego revisadas y aprobadas por un comite de revision de actividades designado por los organizadores del evento. Cada actividad tiene asociado por lo tanto un estado actual que puede ser: Pendiente, Aceptada o Rechazada. 
La app de actividades dispone de un formulario de Propuesta de Actividades que permite a los disertantes indicar el título de la actividad, descripción, el tipo (Charla, Taller, Debate,etc), tema, nivel recomendado de los asistentes (Principiante, Intermedio, Avanzado), cupo máximo y las slides de la presentación en caso de disponer de unas. Son los administradores los encargados de aprobar estas actividades y asignarles un dia, un horario, un espacio y modificar en los datos que considere necesario antes de su publicación. 
Este módulo cuenta también con la administración de las duraciones y temas de las actividades. Ambas funcionalidades estarán disponibles para que el administrador pueda dar de alta, modificar o eliminar según el criterios de los organizadores del evento.



### 3. Proyecto Eventpy

#### 3.1. Descripción del Proyecto

Se desarrollará un sistema web para la gestión de conferencias. El mismo tiene como objetivo coordinar los elementos que intervienen en este tipo de eventos, facilitando la organización y desarrollo del mismo. Los organizadores podrán cargar en la misma las
novedades, realizar llamado a propuestas de charlas, gestionar y administrar la asignación de espacios para cada una de las charlas aceptadas y disponer del registro de todos los asistentes que se inscriban para participar de la conferencia.

El sistema debe contar con los perfiles de: Asistentes, Disertantes (aquellos que propongan charlas) y los Administradores/Organizadores que serán los encargados de publicar las charlas y/o novedades. Las charlas deben poder votarse para futuras estadísticas y debe permitir a cada asistente armar su propio cronograma con las charlas a las que está interesado asistir. Esta última característica permitirá a los organizadores contar con un número estimado de personas que pueden llegar a asistir a una charla particular. Además cada charla tendrá asociado un espacio físico, aula o auditorio con una capacidad máxima y características con la que cuenta (proyector, máquinas, pizarra, etc). 

Las charlas son propuestas por los disertantes y contarán con una descripción y posibles campos que luego serán cargados como por ejemplo el video de la misma una vez finalizado el evento o las diapositivas suministradas por el orador. 

Los organizadores especificarán cuáles de las charlas propuestas son aceptadas y estas podrán luego ser publicadas con su correspondiente horario, día y aula asignada para que los asistentes puedan inscribirse. 

#### 3.2. Objetivos o Propósito

El objetivo del sistema es dar soporte a la realización y organización de eventos, brindando funcionalidades que permitan la administración de  las cuestiones conocidas que involucran este tipo de actividades. 

### 3.3. Beneficios

El sistema pretende ser de utilidad para la organización de eventos y que sirva al mismo tiempo para difundir, presentar y publicar las actividades que se llevarán a cabo; así como también una guía para los organizadores con funcionalidades que le permitan una mejor coordinación de las tareas que deben llevarse a cabo para organizar un evento. 

### 3.4. Alcance

El proyecto abarca el desarrollo de las funcionalidades principales del sistema para organizar un evento. 

Se pretende que el sistema cuente con funcionalidades principales como:

- Gestionar todos los datos asociados a un evento. 
- Que permita administrar el llamado a charlas, brindando un espacio donde los posibles disertantes ingresen sus propuestas. 
- La aprobación y publicación de las charlas aceptadas
- La gestión de todos los aspectos que involucran una charla en sí, como el aula donde se realizará, el tipo de charla, a qué público está dirigida, el tipo y  los temas que involucran la misma. 
- La posibilidad de que los interesados en asistir puedan anotarse. 
- Contar con los datos de la cantidad de asistentes a cada charla. 
- Ser una guía con aquellos hitos importantes comunes a la mayoría de los eventos que sirva a los organizadores de la misma. 

### 3.5. Limitaciones

El proyecto está orientado a eventos del tipo conferencias, específicamente relacionados con el área de sistemas. Como únicamente se cuenta con conocimiento de este tipo de conferencias, se desconoce si eventos de otras áreas presentan características muy distintas que puedan ocasionar que el sistema propuesto no se adapte a sus necesidades. 

### 4. Líneas futuras

A continuación se presentan futuras características que podrían ser incorporadas al sistema con el objetivo de ampliar su funcionalidad en versiones posteriores. 



#### 4.1. Coordinador de Tareas pendientes
El sistema podría incorporar la funcionalidad que permita a los organizadores del evento coordinar las tareas pendientes de un evento particular. Este módulo permitirá dar de alta tareas, definir una fecha límite de finalización, asignar un encargado, determinar un estado, etc. 
    
#### 4.2. Coordinador de Viajes
Para una futura versión el sistema podría permitir a los usuarios especificar su lugar de origen y el medio de transporte que pretende utilizar para asistir con el objetivo de poder coordinar con otros asistentes que viajen desde orígenes cercanos. En caso de que el asistente viaje en auto, por ejemplo, podría especificar si cuenta con lugares disponibles para que otros se sumen con el posible objetivo de compartir gastos, lo cual queda a criterio del dueño del vehículo. La app solo los pone en contacto y actualiza la disponibilidad del vehículo en caso de que exista un acuerdo, esta actualización es realizada por el dueño del vehículo.  

#### 4.3. Coordinador de Alojamiento 
Además, similar a la característica de coordinación de viajes para asistir al evento, los asistentes pueden especificar en qué lugar planean quedarse a dormir en caso de que el evento así lo requiera y de esta manera juntarse con otros asistentes que van al mismo evento. 

#### 4.4. Coordinador de Comidas
Durante el evento puede surgir la necesidad de querer coordinar lugares para ir a comer, la aplicación permitirá especificar a qué lugar planean ir a comer los asistente/disertantes para que los demás puedan sumarse. 

#### 4.5. Disponibilidad en Móviles
Además, se puede pensar en el desarrollo de una aplicación móvil que pueda ser instalada en los teléfonos de los asistentes, disertantes y/o organizadores para tener acceso a las funciones del sistema. Además, podrían agregarse nuevas funcionalidades que sean compatibles con este tipo de dispositivos. 

#### 4.6. Integración con Telegram
El crecimiento actual de telegram y su gran utilidad en el uso de estos eventos para comunicar detalles importantes a los asistentes pueden abrir líneas futuras para integrar telegram con esta aplicación. 

#### 4.7. Integración con redes sociales
Integrar distintas cuentas de redes sociales para la difusión de noticias y novedades como Twitter, Facebook, Instagram, etc. facilitando de esta manera a los organizadores la difusión del evento.







