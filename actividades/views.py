from django.shortcuts import render, redirect
from .forms import PropuestaActividadForm

from eventos.models import Evento
from usuarios.models import Disertante

def registrar_actividad(request):
    if request.method == "POST":
        form = PropuestaActividadForm(request.POST)
        if form.is_valid():
            actividad = form.save(commit=False)
            actividad.disertante = request.user.disertante
            actividad.save()
            return redirect('panel')
    else:
        try:
            disertante = request.user.disertante
            form = PropuestaActividadForm()
        except Exception as e:
            return redirect('disertante')
    
    return render(request, 'actividades/proponer_actividad.html', {'form': form, 'Evento':Evento.objects.all()[0]})

