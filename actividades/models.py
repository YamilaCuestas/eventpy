from django.db import models
from espacios.models import Espacio
from usuarios.models import Disertante, Organizador, User
from eventos.models import Dia



class Duracion(models.Model):
    class Meta:
        verbose_name_plural = 'Duraciones'
    duracion = models.CharField("Duración", max_length=50, unique=True, blank=False)

    def __str__(self):
        return self.duracion

class Tema(models.Model):
    tema = models.CharField("Tema General", max_length=50, unique=True, blank=False)
    descripcion = models.CharField("Descripción", max_length=300, blank=True)

    def __str__(self):
        return self.tema

class Actividad(models.Model):

    class Meta:
        verbose_name_plural = 'Actividades'

    NIVEL = (
        ('Principiante', 'Principiante'),
        ('Intermedio','Intermedio'),
        ('Avanzado','Avanzado'),
        ('No Aplica','No Aplica'),
        )

    ESTADO = (
        ('Pendiente', 'Pendiente'),
        ('Aceptada','Aceptada'),
        ('Rechazada','Rechazada'),
        )
    TIPO = (
        ('Charla', 'Charla'),
        ('Taller','Taller'),
        ('Dabate','Debate'),
        ('Cena','Cena'),
        ('Almuerzo','Almuerzo'),
        ('Sprint','Sprint'),
        ('Otra','Otra'),
        )

    IDIOMA = (
        ('Español', 'Español'),
        ('Ingles', 'Ingles'),
        ('Otro', 'Otro'),
        )

    titulo = models.CharField("Titulo de la Actividad", max_length=100, blank=True, null=True) 
    tipo = models.CharField("Tipo", choices=TIPO, default="Charla", blank=True, max_length=20)
    disertante = models.ForeignKey(Disertante, blank=True, null=True, default=None)
    descripcion = models.TextField("Descripción")
    nivel = models.CharField("Nivel", choices=NIVEL, default="No Aplica", blank=True, max_length=50)
    duracion = models.ForeignKey(Duracion, blank=True, null=True)
    tema = models.ForeignKey(Tema, blank=True, null=True)
    idioma = models.CharField("Idioma", choices=IDIOMA, default="Español", blank=True, max_length=50)
    slides = models.URLField("Slides de la charla propuesta",  blank=True, null=True)
    cupo_max = models.IntegerField( blank=True, null=True)
    
    # A completar por un organizador Administrador
    dia = models.ForeignKey(Dia, blank=True, null=True)
    fecha = models.DateTimeField( blank=True, null=True)
    espacio = models.ForeignKey(Espacio,blank=True, null=True)
    responsable = models.ForeignKey(Organizador, blank= True, null=True)
    link_video = models.URLField("Enlace del video", blank=True, null=True)

    estado = models.CharField("Estado", choices=ESTADO, default="Pendiente", blank=True, max_length=50)

    def __str__(self):
        return self.titulo
