# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-16 15:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('espacios', '0002_auto_20171213_1330'),
        ('usuarios', '0004_auto_20171216_1227'),
    ]

    operations = [
        migrations.CreateModel(
            name='Actividad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(blank=True, max_length=50, null=True, verbose_name='Titulo de la Actividad')),
                ('descripcion', models.TextField(verbose_name='Descripción')),
                ('nivel', models.CharField(blank=True, choices=[('Principiante', 'Principiante'), ('Intermedio', 'Intermedio'), ('Avanzado', 'Avanzado'), ('No Aplica', 'No Aplica')], default='No Aplica', max_length=50, verbose_name='Nivel')),
                ('idioma', models.CharField(blank=True, max_length=50, verbose_name='Idioma')),
                ('slides', models.URLField(blank=True, null=True, verbose_name='Slides de la charla propuesta')),
                ('cupo_max', models.IntegerField(blank=True, null=True, verbose_name='Cupo maximo de asistentes permitidos')),
                ('fecha', models.DateTimeField(blank=True, null=True)),
                ('link_video', models.URLField(blank=True, null=True, verbose_name='Enlace del video')),
                ('estado', models.CharField(blank=True, choices=[('Pendiente', 'Pendiente'), ('Aceptada', 'Aceptada'), ('Rechazada', 'Rechazada')], default='Pendiente', max_length=50, verbose_name='Estado')),
                ('Disertante', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='Disertante', to='usuarios.Disertante')),
            ],
        ),
        migrations.CreateModel(
            name='Duracion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('duracion', models.CharField(max_length=50, unique=True, verbose_name='Duración')),
            ],
        ),
        migrations.CreateModel(
            name='Tema',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tema', models.CharField(max_length=50, unique=True, verbose_name='Tema General')),
                ('descripcion', models.CharField(blank=True, max_length=300, verbose_name='Descripción')),
            ],
        ),
        migrations.AddField(
            model_name='actividad',
            name='duracion',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='actividades.Duracion'),
        ),
        migrations.AddField(
            model_name='actividad',
            name='espacio',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='espacios.Espacio'),
        ),
        migrations.AddField(
            model_name='actividad',
            name='responsable',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='usuarios.Organizador'),
        ),
        migrations.AddField(
            model_name='actividad',
            name='tema',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='actividades.Tema'),
        ),
    ]
