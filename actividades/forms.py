from django import forms
from django.contrib.auth.models import User

from .models import Actividad, Duracion, Tema



class PropuestaActividadForm(forms.ModelForm):
    titulo = forms.CharField(label="Titulo de la Actividad") 
    tipo = forms.ChoiceField(choices = Actividad.TIPO,label="Tipo de Actividad", required=False)  
    descripcion = forms.CharField(label="Descripción", widget=forms.Textarea,required=False)
    nivel = forms.ChoiceField(choices = Actividad.NIVEL,label="Nivel", required=False)
    duracion = forms.ModelChoiceField(queryset=Duracion.objects.all(),required=False)
    tema = forms.ModelChoiceField(queryset=Tema.objects.all(),required=False)
    idioma = forms.ChoiceField(choices = Actividad.IDIOMA,label="Idioma", required=False)
    slides = forms.URLField(label="Slides de la charla propuesta", initial='http://', required=False)
    cupo_max = forms.IntegerField(label="Cupo maximo de asistentes permitidos", required=False)

    class Meta:
        model = Actividad
        fields = ('titulo','tipo','descripcion','nivel','duracion','tema','idioma', 'slides', 'cupo_max')