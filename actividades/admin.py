from django.contrib import admin
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter

from .models import Duracion, Tema, Actividad
from usuarios.models import Disertante
from eventos.models import Dia


admin.site.register(Duracion)


class DisertanteFilter(admin.RelatedFieldListFilter):
    def has_output(self):
        return True

class DiaFilter(admin.RelatedFieldListFilter):
    def has_output(self):
        return True

@admin.register(Actividad)
class ActividadAdmin(admin.ModelAdmin):
    list_display = ('titulo','tipo','nivel','tema','disertante','duracion','dia','espacio','estado')
    search_fields = ('titulo',)
    list_filter = (
        ('estado', DropdownFilter),
        ('tipo',DropdownFilter),
        ('nivel',DropdownFilter),
        ('dia',RelatedDropdownFilter),
        ('disertante',RelatedDropdownFilter),
        )
    ordering = ['titulo']

@admin.register(Tema)
class TemaAdmin(admin.ModelAdmin):
    list_display = ('tema','descripcion')
    search_fields = ('tema',)

