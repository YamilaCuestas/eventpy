from django.conf.urls import url, include
from . import views


urlpatterns = [
	url(r'^$', views.registrar_actividad, name='propuesta_actividad'),
    url(r'^propuesta/$', views.registrar_actividad, name='propuesta_actividad'),
]
    