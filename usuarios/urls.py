from django.conf.urls import url, include
from . import views

from django.contrib.auth import views as auth_views



urlpatterns = [
	url(r'^$', auth_views.login, {'template_name': 'usuarios/login.html'}, name='ingresar'),
    url(r'^ingresar/$', auth_views.login, {'template_name': 'usuarios/login.html'}, name='ingresar'),
    url(r'^salir/$', auth_views.logout, {'next_page': 'inicio'}, name='salir'),
    url(r'^registrar/$', views.registrar, name='registrar'),
    url(r'cambiar/$', views.cambiar_password, name='cambiar'),
    url(r'organizador/$', views.registrar_organizador, name='organizador'),
    url(r'disertante/$', views.registrar_disertante, name='disertante'),
    url(r'disertantes/(?P<disertante>[0-9]+)$', views.ver_disertante, name='ficha_disertante'),
]

