# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-16 15:43
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0004_auto_20171216_1227'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='organizador',
            options={'verbose_name_plural': 'Organizadores'},
        ),
    ]
