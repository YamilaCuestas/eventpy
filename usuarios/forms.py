from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Organizador, Disertante


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(label='Nombre',max_length=30, required=False, help_text='Opcional')
    last_name = forms.CharField(label='Apellido', max_length=30, required=False, help_text='Opcional')
    email = forms.EmailField(label='Email', max_length=254, help_text='Requiere una dirección de correo valida')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )


class OrganizadorForm(forms.ModelForm):
    alias = forms.CharField(label="Nombre o Alias para mostrar", max_length=30)
    twitter = forms.CharField(label="Twitter", max_length=30, required=False)
    #foto_perfil = forms.ImageField(label= "Foto de Perfil",  required=False)
    telefono = forms.CharField(label="Telefono de contacto", max_length=30,  required=True)
    ayuda = forms.CharField( widget=forms.Textarea, label="Contanos en que nos podes ayudar")

    class Meta:
        model = Organizador
        fields = ('alias','twitter', 'foto_perfil', 'telefono', 'ayuda')

class DisertanteForm(forms.ModelForm):
    alias = forms.CharField(label="Nombre o Alias para mostrar", max_length=30)
    ocuapacion = forms.CharField(label="Ocuapación",max_length=30,required=False)
    twitter = forms.CharField(label="Twitter", max_length=30, required=False)
    #foto_perfil = forms.ImageField(label= "Foto de Perfil",  required=False)
    telefono = forms.CharField(label="Telefono de contacto", max_length=30,  required=True)
    bio = forms.CharField( widget=forms.Textarea, label="Bio")

    class Meta:
        model = Disertante
        fields = ('alias', 'ocuapacion','twitter', 'foto_perfil', 'telefono', 'bio')
