from django.contrib import admin

from .models import Disertante, Organizador


@admin.register(Disertante)
class DisertanteAdmin(admin.ModelAdmin):

    list_display = ('user','id','alias','foto_perfil','ocupacion','telefono','habilitado',)
    search_fields = ('nombre',)
    list_filter = ('habilitado',)
    ordering = ['alias']



@admin.register(Organizador)
class OrganizadorAdmin(admin.ModelAdmin):

    list_display = ('user','alias','foto_perfil','cargo','telefono','habilitado',)
    search_fields = ('nombre',)
    list_filter = ('habilitado',)
    ordering = ['alias']


