from django.shortcuts import render
from django.http import HttpResponse

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib.auth.forms import PasswordChangeForm

from .forms import SignUpForm, OrganizadorForm, DisertanteForm
from eventos.models import Evento
from .models import Disertante
from actividades.models import Actividad


def registrar(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('inicio')
    else:
        form = SignUpForm()
    return render(request, 'usuarios/registro.html', {'form': form, 'Evento':Evento.objects.all()[0]})


def cambiar_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Su contraseña fue modificada con exito!')
            return redirect('cambiar')
        else:
            messages.error(request, 'Error, intente nuevamente')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'usuarios/password.html', {'form': form })


def registrar_organizador(request):
    if request.method == "POST":
        form = OrganizadorForm(request.POST, request.FILES)
        if form.is_valid():
            organizador = form.save(commit=False)
            organizador.user = request.user
            organizador.save()
            return redirect('panel')
    else:
        form = OrganizadorForm()

    return render(request, 'usuarios/organizador.html', {'form': form, 'Evento':Evento.objects.all()[0]})


def registrar_disertante(request):
    if request.method == "POST":
        form = DisertanteForm(request.POST, request.FILES)
        if form.is_valid():
            disertante = form.save(commit=False)
            disertante.user = request.user
            disertante.save()
            return redirect('propuesta_actividad')
    else:
        form = DisertanteForm()

    return render(request, 'usuarios/disertante.html', {'form': form, 'Evento':Evento.objects.all()[0]})


def ver_disertante(request, disertante=None):
    print(disertante)
    disertante_obj = Disertante.objects.get(pk=disertante, habilitado=True)

    actividades = Actividad.objects.filter(disertante_id=disertante, estado='Aceptada')
    if (disertante_obj):
        return render(request, 'usuarios/ficha_disertante.html', {'Disertante': disertante_obj, 'actividades': actividades})
    else:
        redirect('inicio')
