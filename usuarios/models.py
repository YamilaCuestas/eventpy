from django.db import models
from django.contrib.auth.models import User


class Disertante(models.Model):
    user = models.OneToOneField(User, unique=True, on_delete=models.CASCADE)
    alias = models.CharField("Nombre o Alias para mostrar", max_length=30, blank=True)
    ocupacion = models.CharField(max_length=30, blank=True)
    bio = models.TextField(blank=True)
    twitter = models.CharField(max_length=30, blank=True)
    foto_perfil = models.ImageField("Foto de Perfil",  blank=True, upload_to='disertantes/')
    telefono = models.CharField("Telefono de contacto", max_length=30, blank=True)
    habilitado = models.BooleanField(blank=False, default=False)

    def __str__(self):
        return self.alias


class Organizador(models.Model):
    class Meta:
        verbose_name_plural = 'Organizadores'

    user = models.OneToOneField(User, unique=True, on_delete=models.CASCADE)
    alias = models.CharField("Nombre o Alias para mostrar", max_length=30, blank=True)
    cargo = models.CharField(max_length=30, blank=True)
    twitter = models.CharField(max_length=30, blank=True)
    foto_perfil = models.ImageField("Foto de Perfil",  blank=True, upload_to='organizadores/')
    telefono = models.CharField("Telefono de contacto", max_length=30, blank=True)
    ayuda = models.TextField(max_length=500, blank=True)
    habilitado = models.BooleanField(blank=False, default=False)

    def __str__(self):
        return self.alias

