# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-11-23 17:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eventos', '0002_evento_id_evento'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evento',
            name='logo',
            field=models.ImageField(blank=True, null=True, upload_to='', verbose_name='Logo del Evento'),
        ),
    ]
