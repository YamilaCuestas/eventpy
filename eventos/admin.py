from django.contrib import admin
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter

from .models import Evento, Dia


admin.site.register(Dia)



@admin.register(Evento)
class EventoAdmin(admin.ModelAdmin):
	list_display = ('nombre','sub_titulo','lugar','llamado_charlas','inscripcion_habilitada','publicar_cronograma')
	fields = ('nombre','sub_titulo','descripcion',('lugar','ciudad'),'correo','cupo',
		'logo','imagen_index_fondo',
		('llamado_charlas','inscripcion_habilitada','publicar_cronograma'),
		('mostrar_disertantes_index','mostrar_organizadores_index','mostrar_sponsors_index'))

	def has_delete_permission(self, request,obj=None):
		return False
	def has_add_permission(self, request,bj=None):
		return False

