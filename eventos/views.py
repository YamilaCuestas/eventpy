from django.shortcuts import render
from django.http import HttpResponse

from .models import Evento, Dia
from sponsors.models import Sponsor
from usuarios.models import User, Organizador, Disertante
from actividades.models import Actividad, Duracion, Tema



def sponsor_view():
    sponsors = Sponsor.objects.filter(habilitado=True)
    if sponsors:
        return sponsors
    else:
        return False

def disertantes_view():
    disertantes = Disertante.objects.filter(habilitado=True)
    if disertantes:
        return disertantes
    else:
        return False

def organizadores_view():
    organizadores = Organizador.objects.filter(habilitado=True)
    if organizadores:
        return organizadores
    else:
        return False

def dias_view():
    dias = Dia.objects.all()
    if disertantes:
        return disertantes
    else:
        return False


def index(request):
    # Toma el primer y unico evento
    evento_actual = Evento.objects.all()[0]

    try:
        dias = Dia.objects.all()
    except Exception as e:
        dias = False

    # DISERTANTES
    cantidad_disertantes = 6 #Mientras no hay disertantes
    return render(request,
                    'eventos/index.html',
                    {'Evento': evento_actual,
                     'cant_disertantes': cantidad_disertantes,
                     'sponsors': sponsor_view(),
                     'disertantes': disertantes_view(),
                     'organizadores': organizadores_view(),
                     'dias': dias,

                    }
        )



def panel(request):
    try:
        disertante = request.user.disertante
        actividades = Actividad.objects.filter(disertante=disertante)
        es_disertante = True
    except Exception as e:
        es_disertante  = False
        actividades = None
    return render(request, 'eventos/panel.html',{'Evento':Evento.objects.all()[0], 'es_disertante': es_disertante, 'actividades': actividades} )


# Plantilla Original del tema
def original(request):
    return render(request, 'eventos/original.html',{'Evento': None,})

# Plantilla Base (temporal)
def base(request):
    return render(request, 'eventos/simple_page.html',{'Evento':Evento.objects.all()[0]})




def cronograma(request):
    dias = Dia.objects.order_by("fecha")
    actividades = Actividad.objects.order_by("fecha")

    return render(request, 'eventos/cronograma.html',{'Evento':Evento.objects.all()[0],'Actividades':actividades, 'Dias':dias } )

