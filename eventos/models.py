from django.db import models

class Evento(models.Model):


    class Meta:
        verbose_name_plural = 'Datos Evento'

    id_evento = models.IntegerField(unique=True, blank=False, null=False, editable=False)
    nombre = models.CharField("Nombre del Evento", max_length=50, blank=True, null=True)
    sub_titulo = models.CharField("subtitulo del Evento", max_length=100, blank=True, null=True)
    descripcion = models.TextField("Descripción del evento")
    lugar = models.CharField("Lugar donde se lleva a cabo", max_length=100, blank=True, null=True)
    correo = models.EmailField(blank=True, null=True)
    maps = models.TextField(blank=True, null=True)
    ciudad = models.CharField(max_length=50, blank=True, null=True)
    fecha_desde = models.DateTimeField( blank=True, null=True)
    fecha_hasta = models.DateTimeField( blank=True, null=True)
    cupo = models.IntegerField(blank=True, null=True)
    logo = models.ImageField("Logo del Evento", upload_to='eventos/',  blank=True, null=True)
    imagen_index_fondo = models.ImageField("Imagen de fondo del index", upload_to='eventos/', blank=True, null=True)

    mostrar_disertantes_index = models.BooleanField(blank=False, default=True)
    mostrar_organizadores_index = models.BooleanField(blank=False, default=True)
    mostrar_sponsors_index = models.BooleanField(blank=False, default=True)
    mostrar_charlas_index = models.BooleanField(blank=False, default=True)

    llamado_charlas = models.BooleanField(blank=False, default=False)
    inscripcion_habilitada = models.BooleanField(blank=False, default=False)
    publicar_cronograma = models.BooleanField(blank=False, default=False)



    def __str__(self):
        return self.nombre



class Dia(models.Model):
    evento = models.ForeignKey(Evento, blank=True, null=True, default=None)
    dia = models.IntegerField(unique=True, blank=True, null=False)
    fecha = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return 'Día %s: %s' %(self.dia, self.fecha.date().strftime("%d-%m-%Y"))
