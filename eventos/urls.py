from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^$', views.index, name='inicio'),
    url(r'^original/$', views.original),
    url(r'^base/$', views.base),
    url(r'^usuarios/', include('usuarios.urls')),
    url(r'^panel/$', views.panel, name='panel'),
    url(r'^actividades/', include('actividades.urls')),
    url(r'^cronograma/', views.cronograma, name='cronograma')
]