from django.db import models
from django.utils import timezone

class Sponsor(models.Model):
	SIN_TIPO = "-"
	ORO = "Oro"
	PLATINO = "Platino"
	PLATA = "Plata"
	TIPO_SPONSORS = (
		(PLATINO,"Platino"),
		(ORO, 'Oro'),
		(PLATA, "Plata"),
		(SIN_TIPO, "-"),
		)
	nombre = models.CharField("Nombre", max_length=50, blank=True, null=True)
	logo = models.ImageField("Logo" ,upload_to='sponsors/' ,blank=True,null=True )
	descripcion = models.CharField("Descripción", max_length=50, blank=True, null=True)
	url = models.URLField("Enlace al Sitio Oficial", max_length=200, blank=True, null=True)
	tipo = models.CharField(max_length=12, choices= TIPO_SPONSORS, default=SIN_TIPO)
	habilitado = models.BooleanField(blank=False, default=False)
	mostrar_nombre = models.BooleanField(blank=False, default=True)
	mostrar_descripcion = models.BooleanField(blank=False, default=False)


	def __str__(self):
		return self.nombre



