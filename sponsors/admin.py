from django.contrib import admin
from .models import Sponsor



@admin.register(Sponsor)
class SponsorAdmin(admin.ModelAdmin):
	list_display = ('nombre','descripcion','url','tipo','habilitado')
	search_fields = ('nombre',)
	ordering = ['nombre']
	list_filter = ['habilitado','tipo', ]
