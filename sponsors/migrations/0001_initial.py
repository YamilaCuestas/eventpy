# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-13 15:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sponsor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank=True, max_length=50, null=True, verbose_name='Nombre')),
                ('logo', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Logo del Evento')),
                ('descripcion', models.CharField(blank=True, max_length=50, null=True, verbose_name='Descripción')),
                ('url', models.URLField(blank=True, null=True, verbose_name='Enlace del Sitio')),
                ('tipo', models.CharField(choices=[('Oro', 'Oro'), ('Diamante', 'Diamante'), ('Plata', 'Plata'), ('-', '-')], default='-', max_length=12)),
                ('habilitado', models.BooleanField(default=False)),
            ],
        ),
    ]
